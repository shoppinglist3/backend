# Server jaja

## Run

For configuration, create the following `.env` file, or set  the environment variables accordingly. Feel free to choose different credentials.
```
# for the postgres docker container
POSTGRES_PASSWORD=pollution1337
POSTGRES_USER=benzin
POSTGRES_DB=diesel

# for the server
DATABASE_URL=postgres://benzin:pollution1337@localhost/diesel
SERVER_ADDRESS=127.0.0.1:1234
RUST_LOG=info
```

Diesel requires rust nightly. Install it using rustup:
```
rustup default nightly
```

Now you can run everything.

```
# running things
docker run --env-file .env -p 5432:5432 -d postgres
cargo run
```

## Protocol

You can connect to the websocket server at port `1234`.

For a description of all message types, see [Messages.md](Messages.md).

### Protocol version

To exchange the protocol versions, a custom http header is used:

```
X-PROTO-VERSION = <version number>
```

The current protocol version is `1`.

Both the client and the server should send this http header to each other. It should be checked, if both protocol versions are still (backwards-)compatible to each other. If not, the connection is to be closed.

The compatibility check between the two versions is carried out like this:

 - If the protocol version sent by the client and by the server are the same, then everything is OK.
 - If the protocol version sent by the client is older than the protocol version of the server, then the compatibility check will be performed on the server side. If the client version is too old for backwards-compatibility, the server will reject the connection. The client should probably prompt the user to upgrade the app, if this happens.
 - If the protocol version sent by the client is newer than the protocol version of the server, then it is the responsibility of the client to check, if it will still be compatible to the server. The server can't do the check in this case, because it might not know about newer versions yet. If the check fails, the connection should be closed again.

For the client implementation, this means:

 - It needs to define its current protocol version and the minimum protocol version of the server, that it is still compatible to.
 - When establishing the connection to the server, it should send its current protocol version using the custom `X-PROTO-VERSION` header.
 - The server might reject the connection at this point. This means, that the client is too old.
 - It should look for the `X-PROTO-VERSION` response header sent by the server. It should check, that it is at least the minimum supported server protocol version.

#### Protocol versions overview

| Version       | Minimum client version     | Minimum server version     | Description |
| :------------- | :----------: | :----------: | -----------: | 
|  1 | 1 | 1 | Initial protocol version |

### Error handling

If an error occurs (e.g. because the client sent an invalid message), then the connection will be closed.

The close frame contains a close code that describes the raw category of the error:

| close code | meaning |
|:----------:|:--------|
| 1011       | Something unexpected has gone wrong. Think of it as a 500 Internal Server Error. See the server log for details on what has gone wrong.|
| 4000       | Client error: The client did something wrong. Things like sending invalid json, or using a non-existing message type are what triggers this error. Clients that are correctly implemented should never see this error. The close frame will also contain a helpful message for the developer. |
| 4001       | Authentication error. The credentials presented in the hello message were incorrect. |

HINT: Use a websocket client, that actually shows the close frame, when testing. [WsCat](https://github.com/websockets/wscat) would be a recommendable choice.

Example: 

```
wscat -c ws://127.0.0.1:1234
 > Connected (press CTRL+C to quit)
 > > {"Hello": {"group": {"Existing": {"uuid": "64f6151c-f5af-4b09-8f50-60b96d381bab", "secret": "xusRCrjz//El0rftfAQS1siyNh9HmKVfCZEfAwjSH8k="}}}}
 > Disconnected (code: 4000, reason: "Invalid message: missing field `version` at line 1 column 141")
```

### Authentication

After connecting, the client needs to send a "Hello" message to the server.
This MUST be the first message, that is sent.
If you re-connect after a connection loss, make sure to also re-send the hello message.

The message contains all credentials needed for authentication.
If the connection fails, the server will close the connection (close code 4001).
There is no special answer message in case of a success - you can just start synchronizing the shopping list.

The first time, you connect to the server, you will need to create a new shared group.
There is an option in the "Hello" message to create a new group instead of authenticating for an existing one.
In this case, you won't need any credentials. The server will respond tho this message once the new group has been 
created with the credentials of the new group.

Once the "Hello" message is sent, it cannot be sent again. If you want to switch to a different group, just open a new 
websocket connection.

### Sharing

To give others access to a group, just share the group uuid and secret. There is no notion of "devices" or "clients".

### Versioning

Every shared group has a version number. The version number increases, whenever changes are made. The server will send you
a new version number for every change. Do not increment the version number yourself - this will lead to synchronisation errors.

It is important to always remember the version number of the group, that you currently have available offline. We will call this your "local version".

The version number is being used for two things:

 - You will need to send your local version as part of the hello message. Based on this, the server figures out, which changes it needs to send to you.
 - Whenever you push changes that the user made to the server, you need to include the version number that those changes where based upon. The server keeps a full history of the group and can use this, to do a proper three-way merge between "your changes" and other changes, that you were not aware of at the time the changes were made. This means, there will never be strange synchronisation issues, even if the client was without internet for quite some time.

The version number for every group starts with 0.
So, in order to do a full synchronization (e.g. to fix sync errors):

 1. Wipe all your local state (except the group credentials).
 2. Set your local version to 0.
 3. Reconnect to the server, send the hello message with group credentials and version 0.
 4. The server will send you a full copy of the current group.

It is probably a good idea, to do all of this within a sql transaction, so that you don't end up with some half-synchronized
state, if the connection breaks.

### Publishing changes to the server

Messages are sent to the server using the [ApplyChanges](Messages.md#message-type-applychanges) message typee.

This message needs to contain a list of `changes` (objects, that were either `Set` to some new value, or `Del` deleted). 

The `base_version` is the local version, that you had when the user made those changes. It is used to figure out, 
how to merge your changes with the changes everyone else made since then. The server will apply your changes on top of 
the base version and then do a three-way-merge between the base version, your changes, and the current version.

Note: For a proper synchronisation, it is important that the base version is indeed your local version at the 
time when the changes were made, not your local version at the time of uploading the changes.

Finally, it contains a `commit_id`. This is a random UUID chosen by you to identify this set of changes. 
When you later see that commit id being pushed back from the server to you, you will know, that your changes have been applied correctly.
Also, the commit id makes your changes idempotent: If you somehow miss the confirmation, that your changes were applied 
successfully, you can simply re-submit the changes, without causing any trouble.