use std::fmt::{self, Display};
use serde::export::Formatter;
use uuid::Uuid;
use indoc::indoc;
use shopping_list_backend::websocket_server::messages::*;
use shopping_list_backend::object_storage::versioned_objects::*;

struct Section {
    message_type: &'static str,
    description: &'static str,
    examples: Vec<Example>
}

struct Example {
    description: &'static str,
    message: Message
}

impl Display for Section {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "## Message type: {}\n", self.message_type)?;
        writeln!(f, "{}\n", self.description)?;
        for example in self.examples.iter() {
            writeln!(f, "{}", example)?;
        }
        Ok(())
    }
}

impl Display for Example {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self.message)
            .map_err(|_|fmt::Error{})?;
        writeln!(f, "{}", self.description)?;
        writeln!(f, "\n```json\n{}\n```", json)
    }
}

fn main() {

    println!(indoc!("
        # Message types documentation

        This file was auto-generated. Use the following command to re-generate it:

        ```
        cargo run --bin print-example-messages > Messages.md
        ```
    "));

    let example_uuid = Uuid::parse_str("73013a8e-2ed0-4ec5-a869-2060917564e9").expect("This is a valid UUID.");


    let hello = Section {
        message_type: "Hello",
        description: "The hello message is the first message, that should be sent by the client to the server.",
        examples: vec![

            Example {
                description: "A new group can be created like this. The server will reply to this with the GroupCreated message containing the credentials for the newly created group:",
                message: Message::Hello(
                    Hello {
                        group: Group::New,
                        version: Version(0),
                    }
                )
            },

            Example {
                description: "To authenticate for an existing group, a group uuid and secret must be provided. If the given credentials are wrong, the connection is closed. If the credentials are correct, the connection stays open and the client can continue sending messages - there is no positive response.",
                message: Message::Hello(
                    Hello {
                        group: Group::Existing(
                            SharedGroupCredentials {
                                uuid: example_uuid,
                                secret: "xusRCrjz//El0rftfAQS1siyNh9HmKVfCZEfAwjSH8k=".to_owned()
                            }
                        ),
                        version: Version(5),
                    }
                )
            },


        ]
    };

    let group_created = Section {
        message_type: "GroupCreated",
        description: "The GroupCreated message is sent from the server to the client, as a response to the Hello message with `group: \"New\"`",
        examples: vec![
            Example{
                description: "Contains the uuid and secret for the newly created group.",
                message: Message::GroupCreated(SharedGroupCredentials{
                    uuid: example_uuid,
                    secret: "xusRCrjz//El0rftfAQS1siyNh9HmKVfCZEfAwjSH8k=".to_string()
                })
            }
        ]
    };
    
    let apply_changes = Section {
        message_type: "ApplyChanges",
        description: "todo",    // todo
        examples: vec![
            Example{
                description: "todo",
                message: Message::ApplyChanges(ChangeSet{
                    commit_uuid: example_uuid,
                    base_version: Version(3),
                    changes: vec![
                        Change::Del { object: ObjectId(example_uuid) },
                        Change::Set { object: ObjectId(example_uuid), new_value: Dummy{
                            some_bool: false,
                            some_number: 0
                        }}
                    ]
                })
            }
        ]
    };

    let diff = Section {
        message_type: "Diff",
        description: "todo",    // todo
        examples: vec![
            Example {
                description: "todo",
                message: Message::Diff(Diff{
                    from_version: Version(0),
                    to_version: Version(3),
                    changes: vec![
                        Change::Set {object: ObjectId(example_uuid), new_value: Dummy {
                            some_bool: false,
                            some_number: 3
                        }},
                        Change::Del {object: ObjectId(example_uuid)}
                    ],
                    commit_uuids: vec![example_uuid],
                })
            }
        ]
    };

    println!("{}", hello);
    println!("{}", group_created);
    println!("{}", apply_changes);
    println!("{}", diff);


}