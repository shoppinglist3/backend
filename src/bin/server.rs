use dotenv::dotenv;
use std::env;
use log::error;
use std::process::exit;
use shopping_list_backend::object_storage::connect::{ConnectionSettings, connect};
use shopping_list_backend::object_storage::migrations::migrate;
use shopping_list_backend::websocket_server::serve;

fn get_env_or_exit(name: &str) -> String {
	env::var(name).unwrap_or_else(|_|{
		error!("Environment variable {} must be set.", name);
		exit(1);
	})
}

#[tokio::main] // By default, tokio_postgres uses the tokio crate as its runtime.
async fn main() {

	// load .env file, if exists
	dotenv().ok();
	
	// logging
	pretty_env_logger::init();

	// get config from environment variables
	let database_url = get_env_or_exit("DATABASE_URL");
	let server_address = get_env_or_exit("SERVER_ADDRESS");

	// run database migrations
	let con_settings = ConnectionSettings::of_database_url(&database_url);
	{
		let (mut db, _) = connect(&con_settings)
			.await
			.unwrap_or_else(|e|{
				error!("Could not connect to the database: {}", e);
				exit(1);
			});
		migrate(&mut db)
			.await
			.unwrap_or_else(|e|{
				error!("Could not run database migrations: {}", e);
				exit(1);
			});
	}

	// run web socket server
	let result = serve(&server_address, &con_settings).await;
	match result {
		Ok(_) => {},
		Err(e) => {
			error!("Server error: {:?}", e);
			exit(1);
		}
	}
}