create table commits (
    shared_group_uuid UUID references shared_groups(uuid),
    commit_uuid UUID,   -- uuid for the commit chosen by the client
    version int,       -- version that was produced when applying this commit
    created_at timestamp with time zone,     -- time stamp - might be useful later, to "garbage collect" old versions
    primary key (shared_group_uuid, commit_uuid)
);
