
create table shared_groups (
	uuid UUID primary key,
	secret varchar not null,             -- (hashed)
	current_version int not null
);


create table objects (
    shared_group_uuid UUID references shared_groups(uuid),
    uuid UUID,
    version_from int,
    version_end int null,
    json varchar,
    primary key (shared_group_uuid, uuid, version_from)
);
