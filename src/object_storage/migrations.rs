use tokio_postgres::{Error, Client};
use log::info;

/// Define all the migrations with name and SQL file here.
const MIGRATIONS: &[(&str, &str)] = &[
    ("01_create_tables", include_str!("migrations_sql/01_create_tables.sql")),
    ("02_create_table_commits", include_str!("migrations_sql/02_create_table_commits.sql")),
];

/// Applies all missing database migrations,
/// to make sure that the database schema is up to date with the application.
pub async fn migrate(client: &mut Client) -> Result<(), Error> {

    // make sure the table for the MIGRATIONS exists
    client.query("CREATE TABLE IF NOT EXISTS MIGRATIONS (name varchar primary key);", &[])
        .await?;

    // run MIGRATIONS
    for (name, sql) in MIGRATIONS.iter() {
        let length = client.query("SELECT name from MIGRATIONS where name = $1", &[name])
            .await?
            .len();
        if length == 0 {
            info!("Apply database migration {:?}", name);
            let transaction = client.transaction().await?;
            transaction.batch_execute(sql).await?;
            transaction.execute("INSERT INTO MIGRATIONS VALUES ($1)", &[name]).await?;
            transaction.commit().await?;
        }
    }

    Ok(())
}
