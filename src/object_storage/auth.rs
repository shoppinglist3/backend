pub use super::versioned_objects::SharedGroup;
use custom_error::custom_error;
use uuid::Uuid;
use rand::prelude::*;
use rand::rngs::OsRng;
use tokio_postgres::Client;
use super::queries;
use crate::object_storage::queries::get_shared_group_by_uuid;
use std::fmt::{Debug, Formatter};
use std::fmt;
use serde::{Serialize, Deserialize};

custom_error!{pub AuthenticationError
    InternalError{why: String} = "Internal error during authentication: {why}",
    Unauthenticated = "Wrong username or password."
}

impl From<argon2::Error> for AuthenticationError {
    fn from(e: argon2::Error) -> Self {
        AuthenticationError::InternalError {
            why: format!("{}", e)
        }
    }
}

impl From<tokio_postgres::Error> for AuthenticationError {
    fn from(e: tokio_postgres::Error) -> Self {
        AuthenticationError::InternalError {
            why: format!("{}", e)
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SharedGroupCredentials {
    pub uuid: Uuid,
    pub secret: String,
}

/// Custom Debug implementation for SharedGroupCredentials, that does not leak the secret.
impl Debug for SharedGroupCredentials {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("SharedGroupCredentials")
            .field("uuid", &self.uuid)
            .field("secret", &"•••••••")
            .finish()
    }
}

pub async fn create_shared_group(con: &Client) -> Result<SharedGroupCredentials, AuthenticationError> {

	// generate a secret
    let mut binary_secret = [0u8; 32];
    OsRng.fill_bytes(&mut binary_secret);
    let secret = base64::encode(binary_secret);

    // generate a random salt
    // a salt is not really necessary, if the secret is already completely random.
    // but argon wants us to use one so fine, we'll just do it then.
    let mut binary_salt = [0u8; 8];
    OsRng.fill_bytes(&mut binary_salt);

    // hash the secret
    let config = argon2::Config::default();
    let hash = argon2::hash_encoded(&binary_secret, &binary_salt, &config)?;

    // generate a random user uuid
    let uuid = Uuid::new_v4();

    // create user
    queries::create_shared_group(con, &uuid, &hash).await?;
    Ok(SharedGroupCredentials{ uuid, secret })
}

pub async fn authenticate(con: &Client, credentials: &SharedGroupCredentials) -> Result<SharedGroup, AuthenticationError> {

    // find the user
    let group = get_shared_group_by_uuid(con, &credentials.uuid).await?;
    let group = match group {
        Some(g) => g,
        None => return Err(AuthenticationError::Unauthenticated),
    };

    // check secret
    let binary_secret = match base64::decode(&credentials.secret) {
        Ok(s) => s,
        Err(_) => return Err(AuthenticationError::Unauthenticated),
    };
    let verified = argon2::verify_encoded(&group.secret, &binary_secret)?;
    if !verified {
        return Err(AuthenticationError::Unauthenticated)
    }

    Ok(group)
}
