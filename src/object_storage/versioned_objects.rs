//! Data structures for working with versioned objects, their histories, and ChangeSets

use uuid::Uuid;
use chrono::{DateTime, Utc};
use std::fmt::Formatter;
use custom_error::custom_error;
use serde::{Serialize, Deserialize};

custom_error!{pub JsonError{} = "Could not parse or format object."}

impl From<serde_json::Error> for JsonError {
	fn from(_: serde_json::Error) -> Self {
		Self{}
	}
}

/// A type implementing [FromRawObject] can be parsed from
/// a raw string representation.
pub trait FromRawObject
	where Self: Sized
{
	fn from_raw(json: &str) -> Result<Self, JsonError>;
}

/// A type that can be serialized into the string representation.
pub trait ToRawObject {
	fn to_raw(&self) -> Result<String, JsonError>;
}

#[derive(Clone)]
pub struct SharedGroup {
	pub uuid: Uuid,
	pub secret: String,
	pub version: Version,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Commit {
	pub commit_uuid: Uuid,
	pub version: i32,
	pub created_at: DateTime<Utc>,
}

/// version number of a group
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct Version(pub i32);

impl Version {

	/// get the version number as an integer
	pub fn number(&self) -> i32 { self.0 }

	/// get the version, that follows this version.
    /// (With incremented version number)
	pub fn next(&self) -> Version { Version(self.number() + 1) }

}

impl std::fmt::Display for Version {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.number())
	}
}

/// a range of versions.
/// Usually used to indicate, which range of versions a value is valid for.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct VersionRange {

	/// The first version, where the value is valid (inclusive).
	pub begin: Version,

	/// The version, that marks the end of the validity period of the value (exclusive).
    /// If it is set to 'None', this means, that it is valid until all eternity.
	pub end: Option<Version>,
}

impl VersionRange {

	pub fn new(begin: Version, end: Option<Version>) -> VersionRange {
		VersionRange{ begin, end}
	}

	pub fn new_open(begin: Version) -> VersionRange {
		VersionRange {
			begin,
			end: None
		}
	}

	pub fn begin(&self) -> Version { self.begin }

	pub fn end(&self) -> Option<Version> { self.end }

	pub fn is_open(&self) -> bool { self.end.is_none() }

	pub fn contains(&self, version: Version) -> bool {
		let is_before_end = match self.end {
			None => true,
			Some(end) => version < end
		};
		let is_after_begin = version >= self.begin;

		is_before_end && is_after_begin
	}
}

/// unique id, referring some object within the group.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ObjectId(pub Uuid);

impl ObjectId {
	pub fn uuid(&self) -> Uuid { self.0 }
}

/// JSON representation of an object.
/// (without any versioning information)
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub(in crate::object_storage) struct RawObjectValue(pub String);

impl RawObjectValue {

	pub fn new(json: String) -> RawObjectValue { RawObjectValue(json) }

	/// Parse the JSON and convert it into an actually usable value.
	pub fn parse<T>(&self) -> Result<T, JsonError>
		where T: FromRawObject {
		T::from_raw(&self.0)
	}

	pub fn json(&self) -> &str { &self.0 }

}

/// An object, as stored in the database, as it has been valid at some point in time.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Object<T>
	where T: Sized {
	pub id: ObjectId,
	pub versions: VersionRange,
	pub value: T
}

/// Shorthand type for objects, that still contain the raw json string.
pub(in crate::object_storage) type RawObject = Object<RawObjectValue>;

/// Raw objects can be "parsed" into proper typed objects.
impl RawObject {

	pub fn parse<T>(&self) -> Result<Object<T>, JsonError>
		where T: FromRawObject {
		Ok(Object {
			id: self.id,
			versions: self.versions,
			value: self.value.parse()?
		})
	}

}

/// A single change of a single object.
/// The "Set" variant indicates, that the object has been created, or the existing one has been updated.
/// The "Del" variant indicates, that the existing object was removed.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum Change<T> {
	Set { object: ObjectId, new_value: T },
	Del { object: ObjectId },
}

impl<T> Change<T> {

	pub fn object(&self) -> ObjectId {
		match self {
			Change::Set { object, .. } => object.clone(),
			Change::Del { object } => object.clone()
		}
	}
}

/// A list of changes that a client has made on top of his offline version.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChangeSet<T> {
	pub commit_uuid: Uuid,
	pub base_version: Version,
	pub changes: Vec<Change<T>>,
}
