use tokio_postgres::{NoTls, Error, Client, AsyncMessage, Notification};
use log::{error, info};
use tokio::sync::mpsc;
use tokio::stream::{Stream, StreamExt};
use tokio::prelude::*;
use futures_util::task::{Context, Poll};
use futures_util::__private::Pin;
use futures_util::__private::Option::Some;

#[derive(Clone)]
pub struct ConnectionSettings {
    database_url: String
}

impl ConnectionSettings {

    pub fn of_database_url(database_url: &str) -> ConnectionSettings {
        ConnectionSettings {
            database_url: database_url.to_owned()
        }
    }

}

struct PostgresMessagesStream<S, T>(tokio_postgres::Connection<S, T>);

impl<S, T> Stream for PostgresMessagesStream<S, T>
    where
        S: AsyncRead + AsyncWrite + Unpin,
        T: AsyncRead + AsyncWrite + Unpin,
{
    type Item = Result<AsyncMessage, tokio_postgres::Error>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let poll = self.0.poll_message(cx);
        poll
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, None)
    }

}



/// Connects to the database
pub async fn connect(connection_settings: &ConnectionSettings) -> Result<(Client, mpsc::Receiver<Notification>), Error> {

    // Connect to the database.
    let (client, connection) = tokio_postgres::connect(
        &connection_settings.database_url,
        NoTls
    ).await?;

    // The connection object performs the actual communication with the database,
    // so spawn it off to run on its own.
    let (notifications_tx, notifications_rx) = mpsc::channel(100);
    let mut connection = PostgresMessagesStream(connection);
    tokio::spawn(async move {
        while let Some(msg) = connection.next().await {
            match msg {
                Ok(AsyncMessage::Notice(notice)) => {
                    info!("{}: {}", notice.severity(), notice.message());
                }
                Ok(AsyncMessage::Notification(message)) => {
                    info!("Postgres Notification: {:?}", message);
                    notifications_tx.send(message)
                        .await
                        .unwrap_or_else(|e|
                            error!("Notification dropped (internal communication): {}", e)
                        );
                }
                Ok(_) => {}
                Err(e) => {
                    error!("Fatal Postgres Error: {}", e);
                    panic!("FATAL POSTGRES ERROR")
                }
            }
        }
    });

    Ok((client, notifications_rx))
}
