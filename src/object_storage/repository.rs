use crate::object_storage::versioned_objects::{SharedGroup, Change, FromRawObject, ToRawObject, ChangeSet, Version, ObjectId, Commit};
use tokio_postgres::{Client, Transaction};
use uuid::Uuid;
use custom_error::custom_error;
use super::queries;
use super::versioned_objects::JsonError;

custom_error!{pub RepositoryError
    DatabaseError{source: tokio_postgres::Error} = "Error while communicating with the database.",
    JsonError{source: JsonError} = "Could not store or load object.",
    MergeError{source: MergeError} = "{source}"
}
custom_error!{pub MergeError{why: String} = "Could not resolve merge conflict: {why}"}

/// An ObjectRepository is the main way of accessing (both read & write) the objects of a group.
/// It brings the ability to access object in a version-controlled manner
pub struct ObjectRepository<'a> {
    group: SharedGroup,
    con: &'a mut Client,
}

pub trait Merge
where Self: Sized
{
    fn merge(base: &Self, left: &Self, right: &Self) -> Result<Self, MergeError>;
}

impl<'a> ObjectRepository<'a> {

    pub fn new(group: &SharedGroup, con: &'a mut Client) -> ObjectRepository<'a> {
        ObjectRepository {
            group: group.clone(),
            con,
        }
    }

    pub async fn apply<T>(&mut self, change_set: &ChangeSet<T>) -> Result<(), RepositoryError>
    where T: FromRawObject + ToRawObject + Merge
    {
        // start transaction
        let transaction = self.con.transaction().await?;

        // lock the group for exclusive access
        let cur_version = queries::get_current_version_and_lock(&transaction, &self.group.uuid)
            .await?;

        // check, if the commit does already exist
        // we will not apply a commit twice.
        let existing_commit = queries::get_commit_by_id(&transaction, &self.group.uuid, &change_set.commit_uuid)
            .await?;
        if let Some(_) = existing_commit {
            return Ok(())   // transaction will be rolled back when it goes out of scope
        }

        // create entry for the commit
        let next_version = cur_version.next();
        let now = chrono::Utc::now();
        queries::insert_commit(&transaction, &self.group.uuid, &change_set.commit_uuid, &next_version.number(), &now)
            .await?;

        // increment the version number
        queries::update_current_version(&transaction, &self.group.uuid, &next_version.number())
            .await?;

        // check, if we can do a "fast forward" (easy)
        if change_set.base_version == cur_version {
            Self::apply_fast_forward(&transaction, &self.group.uuid, &change_set.changes, next_version)
                .await?
        }

        // otherwise, we have to do a more expensive merge.
        else {
            Self::apply_merge(&transaction, &self.group.uuid, &change_set.changes, change_set.base_version, cur_version, next_version)
                .await?;
        }

        // end transaction and release lock
        transaction.commit().await?;
        Ok(())
    }

    async fn apply_fast_forward<T>(
        transaction: &Transaction<'a>,
        group_uuid: &Uuid,
        changes: &Vec<Change<T>>,
        next_version: Version
    ) -> Result<(), RepositoryError>
    where T: ToRawObject
    {
        for change in changes.iter() {
            queries::truncate_end_version(
                transaction,
                group_uuid,
                &change.object().uuid(),
                next_version
            ).await?;

            match change {
                Change::Set { object, new_value } => {
                    let raw = new_value.to_raw()?;
                    queries::insert_object(
                        transaction,
                        group_uuid,
                        &object.uuid(),
                        &next_version.number(),
                        &raw
                    ).await?;
                }
                Change::Del { .. } => { /* Nothing left to do in this case */ }
            }
        }

        Ok(())
    }

    async fn apply_merge<T>(
        transaction: &Transaction<'a>,
        group_uuid: &Uuid,
        changes: &Vec<Change<T>>,
        base_version: Version,
        cur_version: Version,
        next_version: Version
    ) -> Result<(), RepositoryError>
    where T: ToRawObject + Merge + FromRawObject
    {
        for change in changes.iter() {

            // get the object at the base version
            let base_obj = queries::get_object_of_version(
                transaction,
                group_uuid,
                &change.object().uuid(),
                &base_version.number()
            ).await?;

            // get the object at the current version
            // (by checking the end version of base_obj, we can potentially safe a query...)
            let base_still_valid = match &base_obj {
                Some(obj) => obj.versions.contains(cur_version),
                None => false
            };
            let current_obj = if base_still_valid {
                base_obj.clone()
            } else {
                queries::get_object_of_version(
                    transaction,
                    group_uuid,
                    &change.object().uuid(),
                    &cur_version.number()
                ).await?
            };

            // the object after the change
            let updated_obj = match change {
                Change::Set { new_value, .. } => Some(new_value),
                Change::Del { .. } => None
            };

            // merge the three
            let new_object = match (base_obj, updated_obj, current_obj) {

                // object never existed and no one ever created it
                (None, None, None) => None ,

                // Object was created (in either of the versions).
                (None, None, Some(keep)) => Some(keep.value.json().to_owned()),
                (None, Some(keep), None) => Some(keep.to_raw()?),

                // two clients independently created an object with the same id.
                // maybe log a warning? Anyway, the first one wins.
                (None, Some(_), Some(keep)) => Some(keep.value.json().to_owned()),

                // Object was deleted (in any or both versions)
                (Some(_base), None, None) => None ,
                (Some(_base), Some(_), None) => None ,
                (Some(_base), None, Some(_)) => None ,

                // Existing object was changed
                (Some(base), Some(obj_a), Some(obj_b)) => {
                    if base.versions == obj_b.versions {
                        // object was not changed on the server.
                        // we can just take the updated one without further ado
                        Some(obj_a.to_raw()?)
                    } else {
                        // this is the case  that requires a three way merge
                        let obj_b_parsed: T = obj_b.value.parse()?;
                        let base_parsed: T = base.value.parse()?;
                        let merged = T::merge(&base_parsed, obj_a, &obj_b_parsed)?;
                        Some(merged.to_raw()?)
                    }
                }
            };

            // Write the merged object to the database
            queries::truncate_end_version(
                transaction,
                group_uuid,
                &change.object().uuid(),
                next_version
            ).await?;
            if let Some(raw) = new_object {
                queries::insert_object(
                    transaction,
                    group_uuid,
                    &change.object().uuid(),
                    &next_version.number(),
                    &raw
                ).await?;
            }

        }

        Ok(())
    }

    pub async fn diff<T>(&self, left: Version, right: Version) -> Result<Vec<Change<T>>, RepositoryError>
    where T: FromRawObject
    {

        // get the updated / created objects
        let mut changes = queries::get_updates(self.con, &self.group.uuid, &left.number(), &right.number())
            .await?
            .into_iter()
            .map(|it|it.parse())
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .map(|it|Change::<T>::Set { object: it.id, new_value: it.value })
            .collect::<Vec<_>>();

        // get the deleted objects and add them to the list of changes.
        let mut deletions = queries::get_deletions(self.con, &self.group.uuid, &left.number(), &right.number())
            .await?
            .into_iter()
            .map(|uuid|Change::Del {object: ObjectId(uuid)})
            .collect::<Vec<_>>();
        changes.append(&mut deletions);

        Ok(changes)
    }

    pub async fn get_commits(&self, left: Version, right: Version) -> Result<Vec<Commit>, RepositoryError>
    {
        let commits = queries::get_commits(self.con, &self.group.uuid, &left.number(), &right.number())
            .await?;
        Ok(commits)
    }

    pub async fn get_current_version(&self) -> Result<Version, RepositoryError> {
        let v = queries::get_current_version(self.con, &self.group.uuid).await?;
        Ok(v)
    }

    fn get_pubsub_channel_name(&self) -> String {
        format!("shared_groups_{}", self.group.uuid)
    }

    pub async fn publish(&self) -> Result<(), RepositoryError> {
        let notify_channel_name = self.get_pubsub_channel_name();
        let notify_publish_query = format!("notify \"{}\";", notify_channel_name);
        self.con.execute(notify_publish_query.as_str(), &[]).await?;
        Ok(())
    }

    pub async fn subscribe(&self) -> Result<(), RepositoryError> {
        let notify_channel_name = self.get_pubsub_channel_name();
        let notify_listen_query = format!("listen \"{}\";", notify_channel_name);
        self.con.execute(notify_listen_query.as_str(), &[]).await?;
        Ok(())
    }
}
