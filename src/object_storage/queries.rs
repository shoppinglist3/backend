use tokio_postgres::{Client, Error, Row, GenericClient};
use uuid::Uuid;
use super::versioned_objects::SharedGroup;
use crate::object_storage::versioned_objects::{RawObject, ObjectId, VersionRange, RawObjectValue, Version};
use crate::object_storage::versioned_objects::Commit;
use chrono::Utc;


pub async fn create_shared_group(con: &Client, uuid: &Uuid, secret_hash: &str) -> Result<(), Error> {
	con.execute("INSERT INTO shared_groups(uuid, secret, current_version) VALUES($1, $2, 0)", &[uuid, &secret_hash]).await?;
	Ok(())
}

pub async fn get_shared_group_by_uuid(con: &Client, uuid: &Uuid) -> Result<Option<SharedGroup>, Error> {
	let group = con.query_opt("\
		SELECT uuid, secret, current_version FROM shared_groups WHERE uuid = $1
		", &[uuid])
		.await?
		.map(|row| SharedGroup {
			uuid: row.get(0),
			secret: row.get(1),
			version: Version(row.get(2))
		});
	Ok(group)
}

/// Selects the current version of the shared group and at the same time locks that shared
/// group, so that the current transaction has exclusive access to it.
/// Every query from any other transaction trying to modify the same shared group will have to wait until
/// this transaction is committed or rolled back.
pub async fn get_current_version_and_lock<T>(tra: &T, uuid: &Uuid) -> Result<Version, Error>
	where T: GenericClient {
	let version_number = tra.query_one("\
		SELECT current_version
		FROM shared_groups
		WHERE uuid = $1
		FOR UPDATE
		", &[uuid])
		.await?
		.get(0);
	Ok(Version(version_number))
}

pub async fn get_current_version(con: &Client, uuid: &Uuid) -> Result<Version, Error>
{
	let version_number = con.query_one("\
		SELECT current_version
		FROM shared_groups
		WHERE uuid = $1
		", &[uuid])
		.await?
		.get(0);
	Ok(Version(version_number))
}

/// Set current version number for shared group
pub async fn update_current_version<T>(tra: &T, uuid: &Uuid, version: &i32) -> Result<(), Error>
	where T: GenericClient
{
	tra.execute(
		"UPDATE shared_groups SET current_version=$1 WHERE uuid=$2",
		&[version, uuid]
	).await?;
	Ok(())
}

pub async fn truncate_end_version<T>(con: &T, group_uuid: &Uuid, object_uuid: &Uuid, end_version: Version) -> Result<(), Error>
where T: GenericClient
{
	// truncate all version ranges to end latest at `end_version`
	con.execute("\
		UPDATE objects
		SET version_end=$1
		WHERE
			shared_group_uuid=$2
			AND uuid=$3
			AND (
				version_end IS NULL
				OR version_end > $1
			)
		", &[&end_version.0, &group_uuid, &object_uuid])
		.await?;

	// cleanup
	con.execute("\
		DELETE FROM objects
		WHERE
			shared_group_uuid=$1
			AND uuid=$2
			AND version_end <= version_from
		", &[&group_uuid, &object_uuid])
		.await?;
	Ok(())
}

pub async fn insert_object<T>(
	con: &T,
	group_uuid: &Uuid,
	object_uuid: &Uuid,
	version_begin: &i32,
	json: &String
) -> Result<(), Error>
where T: GenericClient
{
	con.execute("\
		INSERT INTO objects(shared_group_uuid, uuid, version_from, version_end, json)
		VALUES($1, $2, $3, NULL, $4)
	", &[group_uuid, object_uuid, version_begin, json])
		.await?;
	Ok(())
}

pub async fn insert_commit<T>(
	con: &T,
	shared_group_uuid: &Uuid,
	commit_uuid: &Uuid,
	version: &i32,
	created_at: &chrono::DateTime<Utc>,
) -> Result<(), Error>
where T: GenericClient
{
	con.execute("\
		INSERT INTO commits(shared_group_uuid, commit_uuid, version, created_at)
		VALUES($1, $2, $3, $4)
	", &[shared_group_uuid, commit_uuid, version, created_at])
		.await?;
	Ok(())
}

pub async fn get_commit_by_id<T>(
	con: &T,
	shared_group_uuid: &Uuid,
	commit_uuid: &Uuid
) -> Result<Option<Commit>, Error>
where T: GenericClient
{
	let maybe_row = con.query_opt("\
		SELECT commit_uuid, version, created_at
		FROM commits
		WHERE shared_group_uuid = $1 AND commit_uuid = $2
	", &[shared_group_uuid, commit_uuid])
		.await?;

	Ok(
		maybe_row.map(|row| Commit {
			commit_uuid: row.get("commit_uuid"),
			version: row.get("version"),
			created_at: row.get("created_at")
		})
	)
}

/// takes a row with the columns in the following order:
///
///  > uuid, version_from, version_end, json
///
/// and constructs a raw object from it.
fn row_to_raw_object(row: &Row) -> RawObject {
	RawObject{
		id: ObjectId(row.get(0)),
		versions: VersionRange{
			begin: Version(row.get(1)),
			end: row.get::<usize, Option<i32>>(2).map(|nr|Version(nr))
		},
		value: RawObjectValue::new(row.get(3))
	}
}

pub(in crate::object_storage) async fn get_object_of_version<T> (
	con: &T,
	group_uuid: &Uuid,
	uuid: &Uuid,
	version: &i32
) -> Result<Option<RawObject>, Error>
where T: GenericClient
{
	con.query_opt("\
		SELECT uuid, version_from, version_end, json
		FROM objects
		WHERE shared_group_uuid = $1
			AND uuid = $2
			AND version_from <= $3
			AND (
				version_end > $3
				OR version_end IS NULL
			)
		", &[group_uuid, uuid, version])
		.await
		.map(|option| option.map(|row|row_to_raw_object(&row)))
}


pub(in crate::object_storage) async fn get_updates(con: &Client, group_uuid: &Uuid, left: &i32, right: &i32) -> Result<Vec<RawObject>, Error> {
	let updates = con.query("\
		SELECT uuid, version_from, version_end, json
		FROM objects
		WHERE shared_group_uuid = $1
			AND version_from <= $2
			AND (
				version_end > $2
				OR version_end IS NULL
			)
			AND version_from > $3
	",&[group_uuid, right, left])
		.await?
		.iter()
		.map(|row| row_to_raw_object(&row))
		.collect();

	Ok(updates)
}

pub async fn get_deletions(con: &Client, group_uuid: &Uuid, left: &i32, right: &i32) -> Result<Vec<Uuid>, Error> {
	// todo: this query is wrong!
	let deleted_uuids = con.query("\
		SELECT uuid
		FROM objects
		WHERE shared_group_uuid = $1
			AND version_from <= $2
			AND version_end > $2
			AND version_end <= $3
	", &[group_uuid, left, right])
		.await?
		.iter()
		.map(|row| row.get(0))
		.collect();

	Ok(deleted_uuids)
}

pub async fn get_commits(con: &Client, group_uuid: &Uuid, left: &i32, right: &i32) -> Result<Vec<Commit>, Error> {
	let commits = con.query("\
		SELECT commit_uuid, version, created_at
		FROM commits
		WHERE shared_group_uuid = $1
			AND version > $2
			AND version <= $3
	", &[group_uuid, left, right])
		.await?
		.iter()
		.map(|row| Commit {
			commit_uuid: row.get("commit_uuid"),
			version: row.get("version"),
			created_at: row.get("created_at")
		})
		.collect();

	Ok(commits)
}