use serde::{Deserialize, Serialize};
use crate::object_storage::versioned_objects::{FromRawObject, ToRawObject, JsonError};
use crate::object_storage::repository::{Merge, MergeError};

#[derive(Serialize, Deserialize, Debug)]
pub struct Dummy {
    pub some_bool: bool,
    pub some_number: i32
}

impl ToRawObject for Dummy {
    fn to_raw(&self) -> Result<String, JsonError> {
        let json = serde_json::to_string(self)?;
        Ok(json)
    }
}

impl FromRawObject for Dummy {
    fn from_raw(json: &str) -> Result<Self, JsonError> {
        serde_json::from_str(json).map_err(|e|e.into())
    }
}

impl Merge for Dummy {
    fn merge(base: &Self, left: &Self, right: &Self) -> Result<Self, MergeError> {
        let result = Self {
            some_bool: match (base.some_bool, left.some_bool, right.some_bool) {
                (_, false, false) => false,
                (_, true, true) => true,
                (false, false, true) => true,
                (false, true, false) => true,
                (true, true, false) => false,
                (true, false, true) => false,
            },
            some_number: left.some_number + right.some_number - base.some_number
        };
        Ok(result)
    }
}