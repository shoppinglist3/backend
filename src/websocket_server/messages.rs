use serde::{Serialize, Deserialize};
use serde::export::fmt::Debug;
use serde::export::Formatter;
use core::fmt;
use std::fmt::Display;

pub use crate::object_storage::auth::SharedGroupCredentials;
pub use crate::object_storage::repository::*;
pub use crate::shopping_list::model::Dummy;
use crate::object_storage::versioned_objects::{ChangeSet, Version, Change};
use uuid::Uuid;

/// This is the base type for all messages.
#[derive(Serialize, Deserialize, Debug)]
pub enum Message {

    /// First message to be sent. Client -> Server.
    Hello(Hello),

    /// Sent to the client as a response to the hello, if a new group was created.
    GroupCreated(SharedGroupCredentials),

    /// Sent from the client to the server to update a group
    ApplyChanges(ChangeSet<Dummy>),

    /// Sent from the server to the client when there are updates
    Diff(Diff)
}

/// Prints a short (1 - line), human readable summary of the message.
impl Display for Message {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Hello(Hello{group: Group::New , .. }) => write!(f, "Hello with create new group"),
            Self::Hello(_) => write!(f, "Hello"),
            Self::GroupCreated(_) => write!(f, "GroupCreated"),
            Self::ApplyChanges(msg) => write!(f, "ApplyChanges on top of version {}", msg.base_version),
            Self::Diff(Diff{from_version, to_version, ..}) => write!(f, "Diff from version {} to version {}", from_version, to_version)
        }
    }
}

/// Sent from the client to the server to "log in" as the first message after the connection is
/// established.
#[derive(Serialize, Deserialize, Debug)]
pub struct Hello {
    pub group: Group,           // the group that the client wants to change or get updates for.
    pub version: Version,  // the version of the group that the client has available offline.
}

/// Choose between creating a new group, or authenticating for an existing one.
#[derive(Serialize, Deserialize, Debug)]
pub enum Group {
    New,
    Existing(SharedGroupCredentials)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Diff {
    pub changes: Vec<Change<Dummy>>,
    pub commit_uuids: Vec<Uuid>,
    pub from_version: Version,
    pub to_version: Version,
}

// todo
// - GdprExport
// - DeleteGroup
// - ApplyChanges