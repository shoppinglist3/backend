use tokio_tungstenite::tungstenite::{Message as WsMessage, Error as WsError, Error};
use crate::websocket_server::messages::{self, Message};
use log::{info, warn, trace};
use std::net::SocketAddr;
use tokio::sync::mpsc;
use crate::websocket_server::sender::SendControlCommand;
use crate::object_storage::connect::ConnectionSettings;
use crate::object_storage;
use custom_error::custom_error;
use crate::object_storage::auth::{create_shared_group, AuthenticationError, authenticate};
use crate::object_storage::repository::{ObjectRepository, RepositoryError};
use tokio::sync::mpsc::error::SendError;
use tokio::stream::{Stream, StreamExt};
use tokio_postgres::Notification;
use crate::websocket_server::messages::Message::Diff;
use crate::object_storage::versioned_objects::Version;

custom_error!{pub ReceiverError
    ClientError{what: String} = "{what}",
    Unauthenticated{} = "Wrong group uuid or secret.",
    InternalError{what: String} = "{what}"


    //DatabaseError{source: tokio_postgres::Error} = "Database error: {source}",
    //DatabaseJsonError{} = "Error while converting object to/from json for storing/reading it to/from the database.",
    //InternalCommunicationError{source: tokio::sync::mpsc::error::SendError<SendControlCommand>} = "Internal communication error: {source}",
    //WebsocketError{source: tokio_tungstenite::tungstenite::Error} = "WebSocket error",
    //HashingError{source: argon2::Error} = "Nah {source}",
    //MergeError{what: String} = "Could not apply changes because of a merge conflict (No sensible client implementation should ever be able to produce a merge conflict. Something is really smelly): {what}"
}

impl From<AuthenticationError> for ReceiverError {
    fn from(e: AuthenticationError) -> Self {
        match e {
            AuthenticationError::InternalError { why } => ReceiverError::InternalError{what: why},
            AuthenticationError::Unauthenticated => ReceiverError::Unauthenticated {}
        }
    }
}

impl From<tokio_postgres::Error> for ReceiverError {
    fn from(e: tokio_postgres::Error) -> Self {
        ReceiverError::InternalError {
            what: format!("Database error: {}", e)
        }
    }
}

impl From<tokio::sync::mpsc::error::SendError<SendControlCommand>> for ReceiverError {
    fn from(e: SendError<SendControlCommand>) -> Self {
        ReceiverError::InternalError {
            what: format!("Internal communication error: {}", e)
        }
    }
}

impl From<tokio_tungstenite::tungstenite::Error> for ReceiverError {
    fn from(e: tokio_tungstenite::tungstenite::Error) -> Self {
        ReceiverError::InternalError {
            what: format!("Websocket error: {}", e)
        }
    }
}

impl From<RepositoryError> for ReceiverError {
    fn from(e: RepositoryError) -> Self {
        match e {
            RepositoryError::DatabaseError { source } => ReceiverError::from(source),
            RepositoryError::JsonError { .. } => ReceiverError::InternalError {
                what: "Error while converting object to/from json for storing/reading it to/from the database.".to_owned()
            },
            RepositoryError::MergeError { source } => ReceiverError::ClientError {
                what: format!("Merging error: {}", source.why)
            }
        }
    }
}

async fn next_message<T>(incoming: &mut T, peer: SocketAddr) -> Result<Option<Message>, ReceiverError>
    where T: Stream<Item=Result<WsMessage, WsError>> + Unpin,
{
    while let Some(msg) = incoming.next().await {
        match msg {

            Err(e) => return Err(ReceiverError::from(e)),

            Ok(WsMessage::Text(payload)) => {
                // Text messages: Deserialize, then return them to be processed.

                // deserialize
                let message = serde_json::from_str(&payload)
                    .map_err(|e| ReceiverError::ClientError {
                        what: format!("Invalid message: {}", e)
                    })?;

                // log
                info!("{}: Received message: {}", peer, message);
                trace!("{}: Full message: {:?}", peer, message);

                // return
                return Ok(Some(message))
            }

            Ok(WsMessage::Close(_)) => {
                // Close message: We'll reply with a close and then tear everything down.
                info!("{}: Closing connection...", peer);
                return Ok(None);
            }

            Ok(_) => {
                // All other message types can be ignored
            }

        }
    }

    info!("{}: Connection closed.", peer);
    Ok(None)
}

async fn send_diff<'a>(repository: &'a ObjectRepository<'a>, client_version: Version, sender_channel_tx: &mpsc::Sender<SendControlCommand>) -> Result<Version, ReceiverError> {
    let current_version = repository.get_current_version().await?;
    if current_version > client_version {
        let diff = repository.diff(client_version, current_version).await?;
        let commits = repository.get_commits(client_version, current_version).await?;
        let msg = Message::Diff(messages::Diff{
            changes: diff,
            commit_uuids: commits.into_iter().map(|c|c.commit_uuid).collect(),
            from_version: client_version,
            to_version: current_version
        });
        sender_channel_tx.send(SendControlCommand::SendMessage(msg)).await?;
    }

    Ok(current_version)
}

enum Event {
    Closed,
    WebSocket(Result<WsMessage, WsError>),
    Notification(Notification)
}

pub async fn receive_task<T>(
    mut incoming: T,
    sender_channel_tx: mpsc::Sender<SendControlCommand>,
    peer: SocketAddr,
    db_con_settings: &ConnectionSettings,
) -> Result<(), ReceiverError>
    where T: Stream<Item=Result<WsMessage, WsError>> + Unpin,
{

    // get the first message (needs to be the hello message
    // get the hello message.
    let first_message = next_message(&mut incoming, peer).await?;
    let hello = match first_message {
        None => return Ok(()),
        Some(Message::Hello(h)) => h,
        Some(_) => {
            return Err(ReceiverError::ClientError {
                what: "The first message must be a 'Hello' message.".to_owned(),
            });
        }
    };

    // database connection
    let (mut db, notifications_rx) = object_storage::connect::connect(db_con_settings).await?;

    // merge incoming websocket messages and database notifications into events stream
    let notifications = notifications_rx
        .map(|n| Event::Notification(n));
    let mut events = incoming
        .map(|m|Event::WebSocket(m))
        .chain(tokio::stream::iter(vec![Event::Closed]))
        .merge(notifications);

    // get or create the group and the credentials
    let credentials = match hello.group {
        messages::Group::New => {
            let credentials = create_shared_group(&db).await?;
            let response = Message::GroupCreated(credentials.clone());
            sender_channel_tx.send(SendControlCommand::SendMessage(response)).await?;
            credentials
        }
        messages::Group::Existing(credentials) => credentials
    };
    let group = authenticate(&db, &credentials).await?;

    // repository to access the versioned objects
    let mut repository = ObjectRepository::new(&group, &mut db);

    // subscribe to postgres notifications
    repository.subscribe().await?;

    // prepare client version with current version and eventually send update
    let mut client_version = hello.version;
    client_version = send_diff(&repository, client_version, &sender_channel_tx).await?;

    // event loop
    while let Some(event) = events.next().await {

        match event {
            Event::Closed => {
                info!("{}: Connection closed.", peer);
                return Ok(())
            }
            Event::WebSocket(Ok(WsMessage::Text(payload))) => {
                // deserialize
                let message = serde_json::from_str(&payload)
                    .map_err(|e| ReceiverError::ClientError {
                        what: format!("Invalid message: {}", e)
                    })?;

                // log
                info!("{}: Received message: {}", peer, message);
                trace!("{}: Full message: {:?}", peer, message);

                // process message
                match message {
                    Message::Hello(_) => {
                        // hello is not to be sent again
                        return Err(ReceiverError::ClientError {
                            what: "The hello message is only allowed as the very first message. You are not allowed to send it twice.".to_owned()
                        });
                    }
                    Message::GroupCreated(_) => {
                        return Err(ReceiverError::ClientError {
                            what: "This message is only supposed to be sent from the server to the client.".to_owned()
                        });
                    }
                    Message::ApplyChanges(changeset) => {
                        let result = repository.apply(&changeset).await?;
                        repository.publish().await?;
                    }
                    Message::Diff(..) => {
                        return Err(ReceiverError::ClientError {
                            what: "This message is only supposed to be sent from the server to the client.".to_owned()
                        });
                    }
                }
            }
            Event::WebSocket(Ok(WsMessage::Close(close))) => {
                match close {
                    Some(close) => info!("{}: Connection closed with {}", peer, close),
                    None => info!("{}: Connection closed with empty close frame.", peer),
                }
                return Ok(())
            }
            Event::WebSocket(Ok(_)) => { /* binary messages, pings and pongs can be ignored. */ }
            Event::WebSocket(Err(e)) => { return Err(ReceiverError::from(e)) }
            Event::Notification(notification) => {
                client_version = send_diff(&repository, client_version, &sender_channel_tx).await?;
            }
        }

    }




    Ok(())

}
