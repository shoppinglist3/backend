use crate::websocket_server::messages::Message;
use tokio::sync::mpsc;
use futures_util::sink::Sink;
use tokio_tungstenite::tungstenite::Message as WsMessage;
use futures_util::SinkExt;
use log::{error, trace, info};
use std::net::SocketAddr;
use std::fmt::Debug;
use tokio_tungstenite::tungstenite::protocol::CloseFrame;
use tokio_tungstenite::tungstenite::protocol::frame::coding::CloseCode;
use std::borrow::Cow;

#[derive(Debug)]
pub enum ErrorCloseCode {
    ClientError = 4000,
    AuthenticationError = 4001,
    InternalError = 1011,
}

#[derive(Debug)]
pub enum SendControlCommand {
    Close,
    CloseError{code: ErrorCloseCode, message: String},
    SendMessage(Message)
}

pub async fn send_task<T>(
    mut outgoing: T,
    mut commands: mpsc::Receiver<SendControlCommand>,
    peer: SocketAddr,
)
    where T: Sink<WsMessage> + Unpin,
          T::Error: Debug
{

    while let Some(command) = commands.recv().await {

        // execute command
        let result = match command {

            // Close
            // After the client already disconnected
            SendControlCommand::Close => {
                info!("{}: Closing connection.", peer);
                let result = outgoing.close().await;
                if let Ok(_) = result {
                    commands.close();
                }
                result
            },

            // Close, with some error message
            SendControlCommand::CloseError { code, message } => {
                info!("{}: Closing connection with error: {}", peer, message);
                outgoing.send(WsMessage::Close(Some(CloseFrame{
                    code: CloseCode::from(code as u16),
                    reason: Cow::from(message)
                }))).await.ok();
                let result = outgoing.close().await;
                if let Ok(_) = result {
                    commands.close();
                }
                result
            },

            // Send an actual message
            SendControlCommand::SendMessage(msg) => {
                info!("{}: Sending message to client: {}", peer, msg);
                trace!("{}: The full message is: {:?}", peer, msg);
                let json = serde_json::to_string(&msg)
                    .expect("It should be possible to serialize every message.");
                outgoing.send(WsMessage::Text(json)).await
            },
        };

        // check errors
        if let Err(e) = result {
            error!("{}: Sending error. Terminating sender. Cause: {:?}", peer, e);
            return;
        }

    }

    info!("{}: Sender has terminated.", peer);

}