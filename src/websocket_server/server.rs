use tokio::net::{TcpListener, TcpStream};
use custom_error::custom_error;
use log::{info, debug, warn, error};
use tokio_tungstenite::{accept_hdr_async};
use std::net::SocketAddr;
use futures_util::{StreamExt};
use tokio_tungstenite::tungstenite::handshake::server::{Request, Response, ErrorResponse};
use tokio_tungstenite::tungstenite::http::{HeaderValue, StatusCode};
use std::str::FromStr;
use tokio::sync::mpsc;
use crate::websocket_server::sender::{send_task, ErrorCloseCode};
use super::sender::SendControlCommand;
use crate::websocket_server::receiver::receive_task;
use crate::object_storage::connect::ConnectionSettings;
use super::receiver::ReceiverError;

const PROTOCOL_VERSION:i32 = 1;
const MIN_CLIENT_PROTOCOL_VERSION:i32 = 1;

custom_error! { pub ServerError
    SocketOpenError{source: std::io::Error} = ""
}

/// Gets the X-PROTO-VERSION header from the client and carries out the version compatibility check.
/// Also, it will inject the X-PROTO-VERSION header into the response, so that the client can check,
/// if it is compatible to the server.
fn client_version_check(req: &Request, mut resp: Response, peer: SocketAddr) -> Result<Response, ErrorResponse> {

    // get the X-PROTO-VERSION header
    let maybe_proto_version = if let Some(header_value) = req.headers().get("X-PROTO-VERSION") {
        if let Ok(header_str) = header_value.to_str() {
            if let Ok(proto_version) = i32::from_str(header_str) {
                Some(proto_version)
            } else { None }
        } else { None }
    } else { None };

    // check the client protocol version
    match maybe_proto_version {
        None => {
            warn!("{}: The X-PROTO-VERSION header is either missing or malformed. Cannot check protocol compatibility.", peer);
        },
        Some(version) => {
            info!("{}: Client speaks protocol version {}", peer, version);
            if version < MIN_CLIENT_PROTOCOL_VERSION {

                // refuse the connection with http status code 400.
                error!("{}: Client is outdated. Refusing connection.", peer);
                let mut too_old_response = ErrorResponse::new(Some(
                    format!("Client protocol version {} is outdated. Need at least protocol version {}.", version, MIN_CLIENT_PROTOCOL_VERSION)
                ));
                *too_old_response.status_mut() = StatusCode::from_u16(400).expect("400 is a valid status code.");
                too_old_response.headers_mut().append("X-PROTO-VERSION", HeaderValue::from(PROTOCOL_VERSION));
                return Err(too_old_response);
            }
        },
    }

    resp.headers_mut().append("X-PROTO-VERSION", HeaderValue::from(PROTOCOL_VERSION));
    Ok(resp)
}

/// Processes an incoming TCP connetion.
async fn handle_connection(stream: TcpStream, peer: SocketAddr, db_con_settings: &ConnectionSettings) {

    // upgrade to websocket connection
    let accepted = accept_hdr_async(
        stream,
        |req: &Request, resp: Response| client_version_check(req, resp, peer)
    ).await;
    let ws_stream = match accepted {
        Ok(s) => s,
        Err(e) => {
            error!("{}: Failed to upgrade to websocket protocol: {}", peer, e);
            return;
        }
    };
    debug!("{}: Established websocket connection.", peer);

    // split the stream, so that we can have separate tasks for sending and receiving simultaneously.
    let (outgoing, incoming) = ws_stream.split();

    // spawn the sender task
    let (sender_tx, sender_rx) = mpsc::channel(100);
    tokio::spawn(async move {
        send_task(outgoing, sender_rx, peer).await;
    });

    // start the receiver in this task
    let result = receive_task(incoming, sender_tx.clone(), peer, db_con_settings).await;
    if let Err(e) = result {

        // log error
        error!("{}: {}", peer, e);

        // disconnect client with error message
        let message = match &e {
            ReceiverError::ClientError {what} => what.clone(),
            ReceiverError::Unauthenticated { .. } => "Wrong uuid or secret.".to_owned(),
            _ => "Internal error".to_owned()
        };
        let code = match &e {
            ReceiverError::ClientError { .. } => ErrorCloseCode::ClientError,
            ReceiverError::Unauthenticated { .. } => ErrorCloseCode::AuthenticationError,
            _ => ErrorCloseCode::InternalError,
        };
        sender_tx.send(SendControlCommand::CloseError{ code, message }).await
            .unwrap_or_else(|e|{
                error!("{}: Internal communication error: {}", peer, e);
            })

    } else {

        // Tell the sender to stop
        sender_tx.send(SendControlCommand::Close).await
            .unwrap_or_else(|e|{
                error!("{}: Internal communication error: {}", peer, e);
            });
        info!("{}: Connection closed.", peer);
    }


}

/// runs the server.
pub async fn serve(addr: &str, db_con_settings: &ConnectionSettings) -> Result<(), ServerError> {

    // listen on the given address
    let listener = TcpListener::bind(addr).await?;
    info!("Listening on: {}", addr);

    // accept connections
    while let Ok((stream, _)) = listener.accept().await {

        // log
        let peer_addr = match stream.peer_addr() {
            Ok(v) => v,
            Err(_) => {
                error!("Connected stream with unknown peer address ??? wtf ?!?");
                continue;
            }
        };
        info!("New connection from {}", peer_addr);

        // upgrade to websocket
        let db_con_settings = db_con_settings.clone();
        tokio::spawn( async move {
            handle_connection(stream, peer_addr, &db_con_settings).await
        });
    }

    Ok(())
}