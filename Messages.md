# Message types documentation

This file was auto-generated. Use the following command to re-generate it:

```
cargo run --bin print-example-messages > Messages.md
```

## Message type: Hello

The hello message is the first message, that should be sent by the client to the server.

A new group can be created like this. The server will reply to this with the GroupCreated message containing the credentials for the newly created group:

```json
{
  "Hello": {
    "group": "New",
    "version": 0
  }
}
```

To authenticate for an existing group, a group uuid and secret must be provided. If the given credentials are wrong, the connection is closed. If the credentials are correct, the connection stays open and the client can continue sending messages - there is no positive response.

```json
{
  "Hello": {
    "group": {
      "Existing": {
        "uuid": "73013a8e-2ed0-4ec5-a869-2060917564e9",
        "secret": "xusRCrjz//El0rftfAQS1siyNh9HmKVfCZEfAwjSH8k="
      }
    },
    "version": 5
  }
}
```


## Message type: GroupCreated

The GroupCreated message is sent from the server to the client, as a response to the Hello message with `group: "New"`

Contains the uuid and secret for the newly created group.

```json
{
  "GroupCreated": {
    "uuid": "73013a8e-2ed0-4ec5-a869-2060917564e9",
    "secret": "xusRCrjz//El0rftfAQS1siyNh9HmKVfCZEfAwjSH8k="
  }
}
```


## Message type: ApplyChanges

todo

todo

```json
{
  "ApplyChanges": {
    "commit_uuid": "73013a8e-2ed0-4ec5-a869-2060917564e9",
    "base_version": 3,
    "changes": [
      {
        "Del": {
          "object": "73013a8e-2ed0-4ec5-a869-2060917564e9"
        }
      },
      {
        "Set": {
          "object": "73013a8e-2ed0-4ec5-a869-2060917564e9",
          "new_value": {
            "some_bool": false,
            "some_number": 0
          }
        }
      }
    ]
  }
}
```


## Message type: Diff

todo

todo

```json
{
  "Diff": {
    "changes": [
      {
        "Set": {
          "object": "73013a8e-2ed0-4ec5-a869-2060917564e9",
          "new_value": {
            "some_bool": false,
            "some_number": 3
          }
        }
      },
      {
        "Del": {
          "object": "73013a8e-2ed0-4ec5-a869-2060917564e9"
        }
      }
    ],
    "commit_uuids": [
      "73013a8e-2ed0-4ec5-a869-2060917564e9"
    ],
    "from_version": 0,
    "to_version": 3
  }
}
```


